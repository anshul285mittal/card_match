using System.Collections;
using UnityEngine.SceneManagement;
using System;
using UnityEngine;

public sealed class GameManager : Singleton<GameManager>
{
    public GameInfo gameInfo;

    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        if (IsAwake)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(Instance.gameObject);
        }
    }
    private void Start()
    {
       
        FirstTimeValueUpdate();
    }

    private void FirstTimeValueUpdate()
    {
        if (!PlayerPrefsHalper.IsFirstTime)
        {
            PlayerPrefsHalper.IsFirstTime = true;
            PlayerPrefsHalper.IsSoundOn = true;
            PlayerPrefsHalper.SoundFXValue = 7;
            PlayerPrefsHalper.MusicValue = 5;
        }
    }
    public void SceneLoad(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    // This Function Use to call any funcnality which related Delay 
    public IEnumerator ActionCallAfterTime(float time, bool isRealTime, Action Call_back)
    {
        if (isRealTime)
        {
            WaitForSecondsRealtime tempObj = new WaitForSecondsRealtime(time);
            yield return tempObj;
            Call_back();
        }
        else
        {
            WaitForSeconds tempObj = new WaitForSeconds(time);
            yield return tempObj;
            Call_back();
        }
    }
}

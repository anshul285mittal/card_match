using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryGameController : MonoBehaviour
{
    public AdjustGridCellSize adjustGridCellSize;
    public GameSave gameSave;

    public GameObject cardPrefab;
    public GameInfo gameInfo;

    private List<CardScript> allCard = new List<CardScript>();
    public List<Sprite> suffledSpriteList { get; private set; }
    public List<bool> CardFlippedData { get; private set; }

    private bool firstClick, secoundClick;
    private CardScript firstClickCard, secoundClickCard;

    

    public bool isGameCompleted {  get; private set; }
    public int currentScore { get; private set; }
    public float currentTimer { get; private set; }
    public int countCorrectCardFlip { get; private set; }

    private float comboTimer;
    private int comboCount=1;

    private bool isComboTimerStart;

    
    void Start()
    {
        if (PlayerPrefsHalper.IsGameSaved)
        {
            CreateSavedBoard();
            currentTimer = PlayerPrefsHalper.SavedGameTimer;
            currentScore = PlayerPrefsHalper.SavedGameCurrentScore;
            countCorrectCardFlip = PlayerPrefsHalper.CountCorrectCardFlip;
        }
        else
        {
            CreateBoard();
            currentTimer = gameInfo.GamePlayTime;
            GamePlayManager.Instance.ScoreUpdate(currentScore);
        }
    }


    private void Update()
    {
       
        if (isGameCompleted)
        {
            return;
        }
        ComboTimer();
        GameTimerLogic();

    }

    // Combo Timer Logic
    private void ComboTimer()
    {
        if (isComboTimerStart)
        {
            comboTimer += Time.deltaTime;
            GamePlayManager.Instance.ComboTimerUpdate(comboCount * 2,
                1-(comboTimer / gameInfo.ComboResetTimer));
            if (comboTimer >= gameInfo.ComboResetTimer)
            {
                isComboTimerStart = false;
                comboCount = 1;
                comboTimer = 0;
                GamePlayManager.Instance.comboObject.SetActive(false);
            }
        }
    }

    //Game Play Timer Logic
    //cuurentTime value get from GameInfo scriptable object.
    private void GameTimerLogic()
    {
        currentTimer -= Time.deltaTime;
        GamePlayManager.Instance.TimerUpdate(TextFormate.getSecToMinNano(currentTimer));
        if (currentTimer < 0)
        {
            GamePlayManager.Instance.LevelFailed();
            isGameCompleted = true;
        }
    }

    //NEW Board Creation
    //Card instance through this Function based on ROW and COLUMN value.
    private void CreateBoard()
    {
        int rowCount = PlayerPrefsHalper.RowCount;
        int columnCount = PlayerPrefsHalper.ColumnCount;
        int totalCardCount = rowCount * columnCount;
       
        suffledSpriteList = CreateSuffuleList(totalCardCount);
        CardFlippedData = new List<bool>();
        for (int i = 0; i < totalCardCount; i++)
        {
            GameObject card = Instantiate(cardPrefab);
            card.transform.SetParent(transform, false);
            card.name = "Card " + i;

            CardScript cardScript = card.GetComponent<CardScript>();
            allCard.Add(cardScript);
            CardFlippedData.Add(false);
            cardScript.cardButton.onClick.AddListener(() => CardSelect(cardScript));

            
            cardScript.mainSprite = suffledSpriteList[i];
            cardScript.CardID = int.Parse(suffledSpriteList[i].name);
        }
        adjustGridCellSize.UpdateCellSize(columnCount,rowCount);
    }

    // OLD Board Creation based on saved value at PlayerPrefs
    private void CreateSavedBoard()
    {
        int rowCount = PlayerPrefsHalper.RowCount;
        int columnCount = PlayerPrefsHalper.ColumnCount;
        int totalCardCount = rowCount * columnCount;

        suffledSpriteList = gameSave.GetSuffuleList();
        CardFlippedData = PlayerPrefsHalper.SavedGameCardFlippedData;
        for (int i = 0; i < totalCardCount; i++)
        {
            GameObject card = Instantiate(cardPrefab);
            card.transform.SetParent(transform, false);
            card.name = "Card " + i;

            CardScript cardScript = card.GetComponent<CardScript>();
            allCard.Add(cardScript);
            cardScript.cardButton.onClick.AddListener(() => CardSelect(cardScript));


            cardScript.mainSprite = suffledSpriteList[i];
            cardScript.CardID = int.Parse(suffledSpriteList[i].name);
           
        }
        adjustGridCellSize.UpdateCellSize(columnCount, rowCount);

        StartCoroutine(GameManager.Instance.ActionCallAfterTime(.03f, true, () => {
            for (int i = 0; i < totalCardCount; i++)
            {
                allCard[i].gameObject.SetActive(!CardFlippedData[i]);
            }
        }));
       
    }

    //Suffling the Sprites.
    private List<Sprite> CreateSuffuleList(int totalCardCount)
    {
        List<Sprite> allSprites = new List<Sprite>();
        for(int i = 0; i < totalCardCount/2; i++)
        {
            allSprites.Add(gameInfo.allCardSprites[i]);
            allSprites.Add(gameInfo.allCardSprites[i]);
        }
       for(int i = 0; i < allSprites.Count; i++)
        {
            Sprite temp = allSprites[i];
            int randomNumber = Random.Range(i, allSprites.Count);
            allSprites[i] = allSprites[randomNumber];
            allSprites[randomNumber] = temp;
        }
        return allSprites;
    }

    //All Card Listener connected to this function.
    //Mange user click first time and secound time.
    public void CardSelect(CardScript cardScript)
    {
        if (!firstClick)
        {
            firstClick = true;
            firstClickCard = cardScript;

            //CardView have all functionality to mange animation of fliping the Card
            cardScript.CardView();
        }
        else if (!secoundClick && firstClickCard!=cardScript)
        {
            secoundClick = true;
            secoundClickCard = cardScript;
    
            cardScript.CardView();
            StartCoroutine(CheckCardMatch());
        }
    }

    //Based on user two choies this function calculate the right/wrong gusses.
    //Score calculation.
    //Combo initiated
    IEnumerator CheckCardMatch()
    {
        yield return new WaitForSeconds(.5f);
        if(firstClickCard.CardID == secoundClickCard.CardID)
        {
            if (isComboTimerStart)
            {
                comboCount *= 2;
                comboTimer = 0;
            }
            else
            {
                isComboTimerStart = true;
            }
            GamePlayManager.Instance.scoreEffect.SetScoreEffect((comboCount * gameInfo.ScoreMultiplier), firstClickCard.transform.position);
            currentScore =  currentScore + (comboCount*gameInfo.ScoreMultiplier);


            //Card Match have functionality to mange Animation and its disable the selected cards.
            firstClickCard.CardMatch();
            secoundClickCard.CardMatch();

            //This data manage for Save Game Logic.
            CardFlippedData[allCard.IndexOf(firstClickCard)] = true;
            CardFlippedData[allCard.IndexOf(secoundClickCard)] = true;

            GamePlayManager.Instance.ScoreUpdate(currentScore);
            CheckGameFinished();
        }
        else
        {
            //Reset the button when user choise wrong
          
            firstClickCard.CardNotMatch();
            secoundClickCard.CardNotMatch();

            if (isComboTimerStart)
            {
                isComboTimerStart = false;
                comboCount = 1;
                comboTimer = 0;
                GamePlayManager.Instance.comboObject.SetActive(false);
            }
        }
        firstClick = false;
        secoundClick = false;
    }
    
    private void CheckGameFinished()
    {
        countCorrectCardFlip++;
        if(countCorrectCardFlip== allCard.Count / 2)
        {
            StartCoroutine(GameManager.Instance.ActionCallAfterTime(.5f, true, () => {
                GamePlayManager.Instance.LevelCompleted(currentScore);
            }));
            isGameCompleted = true;
        }
    }
}

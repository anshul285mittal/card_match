using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public AudioMixer _audioMixer;
  


    private const float MIN_DB_MUSIC = -80, MAX_DB_MUSIC = 0;
    public const float MIN_DB_SFX = -80, MAX_DB_SFX = 0;
    void Awake()
    {
        Debug.Log("Awake");
        if (IsAwake)
        {
            Debug.Log("ISAwake");
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("ISAwake False");
            DontDestroyOnLoad(Instance.gameObject);
        }
    }

    void Start()
    {
        StartCoroutine(GameManager.Instance.ActionCallAfterTime(.1f,true,()=> {
            SetToggleValue(PlayerPrefsHalper.IsSoundOn);
            SetSoundFXValue(PlayerPrefsHalper.SoundFXValue * 10);
            SetMusicValue(PlayerPrefsHalper.MusicValue * 10);
        }));
    }

    // Soound-FX and Music valume manuplate Based on DB value in Audio Mixer
    //DB value -80 is equivelent to 0 Volume and 0 DB value is equivelent to 100 Volume
    public void SetSoundFXValue(float value)
    {
        int MusicValue = (int)value;
        float value2 = 100 - MusicValue;
        value2 *= value2 * value2 / 10000;
        value2 = (100 - value2) / 100;
       
        float final_value = MIN_DB_SFX + ((MAX_DB_SFX - MIN_DB_SFX) * value2);
        _audioMixer.SetFloat("SFX", final_value);
       
    }
    public void SetMusicValue(float value)
    {
        int MusicValue = (int)value;
        float value2 = 100 - MusicValue;
        value2 *= value2 * value2 / 10000;
        value2 = (100 - value2) / 100;
        
        float final_value = MIN_DB_MUSIC + ((MAX_DB_MUSIC - MIN_DB_MUSIC) * value2);

        _audioMixer.SetFloat("GameMusic", final_value);
       
    }
    //Update the Master Group, all updattion directly impacted to sub group(Music and SFX)
    public void SetToggleValue(bool value)
    {
        _audioMixer.SetFloat("Master", value ? 0 : -80);
    }
}

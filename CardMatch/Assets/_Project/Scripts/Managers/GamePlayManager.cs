using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class GamePlayManager : Singleton<GamePlayManager>
{
    public GameSave gameSave;
    [Header("Top Panel Objects")]
    public GameObject pauseScreen;
    public Text scoreTxt;
    public Text timerTxt;

    [Header("Combo Components")]
    public GameObject comboObject;
    public Image comboFillImage;
    public Text comboTxt;

    [Header("Level Completed")]
    public GameObject levelCompletePanel;
    public Text levelCompleteCurrectScoreTxt;
    public Text levelCompleteHighScoreTxt;

    [Header("Level Failed")]
    public GameObject levelFailedPanel;
    public Text levelFailedHighScoreTxt;
    public AudioClip levelFailedClip, levelCompleteClip;

    [Header("Other")]
    public ScoreEffect scoreEffect;

    public void ScoreUpdate(int score)
    {
        scoreTxt.text = score+"";
    }

    public void TimerUpdate(string timer)
    {
        timerTxt.text = timer + "";
    }
    public void ComboTimerUpdate(int comboCount,float timerFillAmount)
    {
        comboObject.SetActive(true);
        comboTxt.text = (comboCount ) + "X";
        comboFillImage.fillAmount = timerFillAmount;
    }
    public void LevelCompleted(int currentScore)
    {
        levelCompletePanel.SetActive(true);
        levelCompleteCurrectScoreTxt.text = currentScore + "";
        if (currentScore > PlayerPrefsHalper.HighScore)
        {
            PlayerPrefsHalper.HighScore = currentScore;
        }
        levelCompleteHighScoreTxt.text = PlayerPrefsHalper.HighScore+"";
        PlayerPrefsHalper.IsGameSaved = false;
        Events.playAudio(levelCompleteClip);
    }
    public void LevelFailed()
    {
        levelFailedPanel.SetActive(true);
        levelFailedHighScoreTxt.text = PlayerPrefsHalper.HighScore + "";
        PlayerPrefsHalper.IsGameSaved = false;
        Events.playAudio(levelFailedClip);
    }
    #region Button Listener 
    public void PauseButton(bool value)
    {
        if (value)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
        pauseScreen.SetActive(value);
    }
    public void ExitGame()
    {
        Time.timeScale = 1;
        gameSave.SavedGame();
        GameManager.Instance.SceneLoad("MainMenu");
    }
    public void Restart()
    {
        Time.timeScale = 1;
        PlayerPrefsHalper.IsGameSaved = false;
        GameManager.Instance.SceneLoad("GamePlay");
    }
    public void OpenMainMenu()
    {
        GameManager.Instance.SceneLoad("MainMenu");
    }
    #endregion

    
}

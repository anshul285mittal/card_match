using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public GameObject settingPanel;
    public GameObject exitPanel;
    public Button continueButton;
    public Text continueBoardSizeTxt;
    private bool isExit;
    void Start()
    {

        continueButton.interactable=PlayerPrefsHalper.IsGameSaved;
        continueBoardSizeTxt.text = PlayerPrefsHalper.RowCount + "X" + PlayerPrefsHalper.ColumnCount;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isExit = !isExit;
            ExitButton(isExit);
        }
    }

    // All Main Menu Button intraction, connect directly to the listener
    #region Button Listener 
    public void CountinueButton()
    {
        GameManager.Instance.SceneLoad("GamePlay");
    }
    public void EasyButton()
    {
        PlayerPrefsHalper.RowCount = 2;
        PlayerPrefsHalper.ColumnCount = 2;
        PlayerPrefsHalper.IsGameSaved = false;
        GameManager.Instance.SceneLoad("GamePlay");
    }
    public void MediumButton()
    {
        PlayerPrefsHalper.RowCount = 2;
        PlayerPrefsHalper.ColumnCount = 3;
        PlayerPrefsHalper.IsGameSaved = false;
        GameManager.Instance.SceneLoad("GamePlay");
    }
    public void HardButton()
    {
        PlayerPrefsHalper.RowCount = 5;
        PlayerPrefsHalper.ColumnCount = 6;
        PlayerPrefsHalper.IsGameSaved = false;
        GameManager.Instance.SceneLoad("GamePlay");
    }
    public void SettingButton(bool value)
    {
        settingPanel.SetActive(value);
    }
   
    public void ExitButton(bool value)
    {
        exitPanel.SetActive(value);
    }
    #endregion

    public void QuitGame()
    {
        Application.Quit();
    }
}

using UnityEngine;
using System.Collections.Generic;

//All PlayerPrefs Should be writen in this Script Only.
//This Class help to manuplate PlayerPrefs.
public sealed class PlayerPrefsHalper 
{

    public static bool IsFirstTime
    {
        get { return PlayerPrefs.GetInt("IsFirstTime") == 1; }
        set { PlayerPrefs.SetInt("IsFirstTime", value ? 1 : 0); }
    }
    public static int RowCount
    {
        get { return PlayerPrefs.GetInt("RowCount"); }
        set { PlayerPrefs.SetInt("RowCount", value); }
    }

    public static int ColumnCount
    {
        get { return PlayerPrefs.GetInt("ColumnCount"); }
        set { PlayerPrefs.SetInt("ColumnCount", value); }
    }
    public static int HighScore
    {
        get { return PlayerPrefs.GetInt("HighScore"); }
        set { PlayerPrefs.SetInt("HighScore", value); }
    }
    public static bool IsSoundOn
    {
        get { return PlayerPrefs.GetInt("IsSoundOn")==1; }
        set { PlayerPrefs.SetInt("IsSoundOn", value?1:0); }
    }

    // Value Should be 0-10.
    public static float SoundFXValue
    {
        get { return PlayerPrefs.GetFloat("SoundFXValue"); }
        set { PlayerPrefs.SetFloat("SoundFXValue", value ); }
    }

    // Value Should be 0-10.
    public static float MusicValue
    {
        get { return PlayerPrefs.GetFloat("MusicValue"); }
        set { PlayerPrefs.SetFloat("MusicValue", value); }
    }

    #region GameSave
    public static bool IsGameSaved
    {
        get { return PlayerPrefs.GetInt("IsGameSaved") == 1; }
        set { PlayerPrefs.SetInt("IsGameSaved", value ? 1 : 0); }
    }
    public static int CountCorrectCardFlip
    {
        get { return PlayerPrefs.GetInt("CountCorrectCardFlip"); }
        set { PlayerPrefs.SetInt("CountCorrectCardFlip", value); }
    }
    public static float SavedGameTimer
    {
        get { return PlayerPrefs.GetFloat("SavedGameTimer"); }
        set { PlayerPrefs.SetFloat("SavedGameTimer", value); }
    }
    public static int SavedGameCurrentScore
    {
        get { return PlayerPrefs.GetInt("SavedGameScore"); }
        set { PlayerPrefs.SetInt("SavedGameScore", value); }
    }
    public static List<string> SavedGameCardData
    {
        get {
            List<string> cardData = new List<string>();
            for(int i=0;i< ColumnCount * RowCount; i++)
            {
                cardData.Add(PlayerPrefs.GetString("SavedGameCardData" + i));
            }
            return cardData; }
        set
        {
            for (int i = 0; i < value.Count; i++)
            {
                PlayerPrefs.SetString("SavedGameCardData" + i, value[i]);
            }
        }
    }
    public static List<bool> SavedGameCardFlippedData
    {
        get
        {
            List<bool> cardFlipedData = new List<bool>();
            for (int i = 0; i < ColumnCount * RowCount; i++)
            {
                cardFlipedData.Add(PlayerPrefs.GetInt("SavedGameCardFlipedData" + i)==1);
            }
            return cardFlipedData;
        }
        set
        {
            for (int i = 0; i < value.Count; i++)
            {
                PlayerPrefs.SetInt("SavedGameCardFlipedData" + i, value[i]?1:0);
            }
        }
    }
    #endregion
}

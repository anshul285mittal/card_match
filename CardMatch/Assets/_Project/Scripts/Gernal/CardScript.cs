using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class CardScript : MonoBehaviour
{
    public Button cardButton;
    public Image cardImage;
    public Sprite backgroundSprite;
    public Sprite mainSprite;
    public int CardID;

    public Animator cardAnimator;

    [Header("Audio Clips")]
    public AudioClip cardFlipClip;
    public AudioClip cardNotMatchClip;
    public AudioClip cardMatchClip;


    private bool isCardView;

    //This function Show the hide card
    public void CardView()
    {
        isCardView = true;
        cardAnimator.Play("CardFlipAnimation", -1);
    }

    //CardMatch is correct then this function show the animations and hide the selected Card
    public void CardMatch()
    {
        cardAnimator.Play("CardMatch", -1);
        Events.playAudio(cardMatchClip);
        StartCoroutine(GameManager.Instance.ActionCallAfterTime(.4f, true, () => {
            gameObject.SetActive(false);
        }));
    }

    //If Card Not mathed then this function play animation and flip back to hide the cards.
    public void CardNotMatch()
    {
        cardAnimator.Play("CardNotMatch", -1);
        Events.playAudio(cardNotMatchClip);
        isCardView = false;
    }

    //This Function call from animation Event.
    //Animation: CardFlip
    public void CardFlipAnimationEvent()
    {
        if (isCardView)
        {
            cardImage.sprite = mainSprite;
        }
        else
        {
            cardImage.sprite = backgroundSprite;
        }
        Events.playAudio(cardFlipClip);
    }
}

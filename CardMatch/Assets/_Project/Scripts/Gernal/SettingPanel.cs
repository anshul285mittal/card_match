using UnityEngine;
using UnityEngine.UI;

public class SettingPanel : MonoBehaviour
{
    public Toggle soundToggle;
    public Slider soundFxSlider;
    public Slider musicSlider;

    private void OnEnable()
    {
        SetUpSettingInfo();
    }

    private void SetUpSettingInfo()
    {
        soundToggle.isOn = PlayerPrefsHalper.IsSoundOn;
        soundFxSlider.value = PlayerPrefsHalper.SoundFXValue;
        musicSlider.value = PlayerPrefsHalper.MusicValue;
    }

    public void SoundToggleUpdate()
    {
        AudioManager.Instance.SetToggleValue(soundToggle.isOn);
    }
    public void SFXVolumeChnageUpdate()
    {
        AudioManager.Instance.SetSoundFXValue(soundFxSlider.value * 10);
    }
    public void MusicVolumeChnageUpdate()
    {
        AudioManager.Instance.SetMusicValue(musicSlider.value * 10);
    }
    public void SettingUpdate()
    {
        PlayerPrefsHalper.IsSoundOn = soundToggle.isOn;
        PlayerPrefsHalper.SoundFXValue = soundFxSlider.value;
        PlayerPrefsHalper.MusicValue = musicSlider.value;

        AudioManager.Instance.SetToggleValue(soundToggle.isOn);
        AudioManager.Instance.SetSoundFXValue(soundFxSlider.value*10);
        AudioManager.Instance.SetMusicValue(musicSlider.value * 10);
    }
    private void OnDisable()
    {
        SetUpSettingInfo();
    }

}

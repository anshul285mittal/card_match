using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//GameSave Class Save the Game-Play status.
//Now We have saved all the Data in the PLayerPrefs.
//As per dificut Data This Class manage data through Json
public class GameSave : MonoBehaviour
{
    public MemoryGameController gameController;

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            if (!gameController.isGameCompleted)
            {
                SavedGame();
            }
        }
    }
    private void OnApplicationQuit()
    {
        if (!gameController.isGameCompleted)
        {
            SavedGame();
        }
    }

    //This Function Save all the date to PlayerPrefs.
    public void SavedGame()
    {
        PlayerPrefsHalper.IsGameSaved = true;
        PlayerPrefsHalper.SavedGameTimer = gameController.currentTimer;
        PlayerPrefsHalper.SavedGameCurrentScore = gameController.currentScore;
        PlayerPrefsHalper.CountCorrectCardFlip = gameController.countCorrectCardFlip;
        List<string> cardData = new List<string>();
        for(int i=0;i<gameController.suffledSpriteList.Count;i++){
            cardData.Add(gameController.suffledSpriteList[i].name);
        }
        PlayerPrefsHalper.SavedGameCardData = cardData;
        PlayerPrefsHalper.SavedGameCardFlippedData = gameController.CardFlippedData;
    }

    //This Function Get the saved List from PlayerPrefs and convert it to Sprites List.
    public List<Sprite> GetSuffuleList()
    {
        List<Sprite> suffledSpriteList = new List<Sprite>();
        List<string> cardData = PlayerPrefsHalper.SavedGameCardData;
        for (int i = 0; i < cardData.Count; i++)
        {
            suffledSpriteList.Add(gameController.gameInfo.allCardSprites[int.Parse(cardData[i]) - 1]);
        }
        return suffledSpriteList;
    }
    
}
